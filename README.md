# sdsget - Synology Download Station Get

sgsget is a wget like command line utility written in Rust.

## Building

sdsget uses Cargo for building. Simply call ``cargo build`` to create an executable.
